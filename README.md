# bottleneck_script

A Macaulay2 script to compute the bottleneck degree of a smooth
projective variety in general position in terms of Chern classes or
polar classes.